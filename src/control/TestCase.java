package control;

import java.util.ArrayList;

import interfaces.Measureable;
import interfaces.Taxable;
import model.BankAccount;
import model.Company;
import model.Country;
import model.Data;
import model.Person;
import model.Product;
import model.TaxCalculator;

public class TestCase {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Measureable[] accounts = new Measureable[3];
		accounts[0] = new BankAccount("Niyuta",0);
		accounts[1] = new BankAccount("Nichapat",10000);
		accounts[2] = new BankAccount("Thamonwan",2000);
		
		System.out.println("Average balance: " + Data.average(accounts));
		BankAccount minBalance = (BankAccount) Data.min(accounts[1], accounts[2]);
		System.out.println("Balance of minimal : " + minBalance.getName() + " " + minBalance.getBalance() + "\n");

		Measureable[] countries = new Measureable[3];
		countries[0] = new Country("Uruguay", 176220);
		countries[1] = new Country("Thailand", 513120);
		countries[2] = new Country("Belgium", 30510);
		
		System.out.println("Average area: " + Data.average(countries));
		Country minArea = (Country) Data.min(countries[1], countries[2]);
		System.out.println("Balance of minimal : " + minArea.getName() + " " + minArea.getArea() + "\n");

		
		Measureable[] persons = new Measureable[3];
		persons[0] = new Person("John", 180, 10000);
		persons[1] = new Person("Mary", 165, 30000);
		persons[2] = new Person("Rick", 185, 8000);
		System.out.println("Average height: " + Data.average(persons));
		Person minHeight = (Person) Data.min(persons[1], persons[2]);
		System.out.println("Balance of minimal : " + minHeight.getName() + " " + minHeight.getHeight() + "\n");
		
		//Tax--------------------------------------------------------------------------------------------------------
		
		System.out.println("Tax----------------------------------------------------");
		
		ArrayList<Taxable> personList = new ArrayList<Taxable>();
		Person person1 = new Person("John", 180, 300000);
		Person person2 = new Person("Mary", 165, 500000);
		Person person3 = new Person("Rick", 185, 1000000);
		personList.add(person1);
		personList.add(person2);
		personList.add(person3);
		System.out.println("Tax for Persons : " + TaxCalculator.sum(personList));
		
		ArrayList<Taxable> companyList = new ArrayList<Taxable>();
		Company company1 = new Company("BAC", 1000000, 800000);
		Company company2 = new Company("Pencing Company", 50000000, 10000000);
		Company company3 = new Company("Franken Company", 400000000, 200500000);
		companyList.add(company1);
		companyList.add(company2);
		companyList.add(company3);
		System.out.println("Tax for Companys : " + TaxCalculator.sum(companyList));
		
		ArrayList<Taxable> productList = new ArrayList<Taxable>();
		Product product1 = new Product("Bag", 30000);
		Product product2 = new Product("Computer", 50000);
		Product product3 = new Product("Table", 4000);
		productList.add(product1);
		productList.add(product2);
		productList.add(product3);
		System.out.println("Tax for Products : " + TaxCalculator.sum(productList));
		
		ArrayList<Taxable> allList = new ArrayList<Taxable>();
		Product product = new Product("Watch", 100000);
		Company company = new Company("Goal Company", 80000000, 53000000);
		Person person = new Person("Peter", 180, 200000);
		allList.add(product);
		allList.add(company);
		allList.add(person);
		System.out.println("Tax for All : " + TaxCalculator.sum(allList));
	
	}

}
