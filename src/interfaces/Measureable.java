package interfaces;

public interface Measureable {

	double getMeasure();
}
