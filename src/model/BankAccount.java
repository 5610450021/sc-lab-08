package model;

import interfaces.Measureable;

public class BankAccount implements Measureable{
	private String name;
	private double balance;
	
	public BankAccount(String name, double balance) {
		this.name = name;
		this.balance = balance;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public double getBalance() {
		return balance;
	}
	public void setBalance(double balance) {
		this.balance = balance;
	}
	@Override
	public double getMeasure() {
		// TODO Auto-generated method stub
		return balance;
	}
	
}
