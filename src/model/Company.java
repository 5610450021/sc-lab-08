package model;

import interfaces.Taxable;

public class Company implements Taxable{
	private String name;
	private double revenue;
	private double expenditure;
	
	public Company(String name, double revenue, double expenditure) {
		this.name = name;
		this.revenue = revenue;
		this.expenditure = expenditure;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public double getRevenue() {
		return revenue;
	}

	public void setRevenue(double revenue) {
		this.revenue = revenue;
	}

	public double getExpenditure() {
		return expenditure;
	}

	public void setExpenditure(double expenditure) {
		this.expenditure = expenditure;
	}

	@Override
	public double getTaxable() {
		double amount = revenue - expenditure;
		double tax = (amount*30)/100;
		return tax;
	}
	
	
	
}
