package model;

import interfaces.Measureable;

public class Country implements Measureable{
	private String name;
	private double area;
	
	public Country(String name, double area) {
		this.name = name;
		this.area = area;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public double getArea() {
		return area;
	}
	public void setArea(double area) {
		this.area = area;
	}
	@Override
	public double getMeasure() {
		// TODO Auto-generated method stub
		return area;
	}
	
	
}
