package model;


import interfaces.Measureable;

public class Data {

	public static double average(Measureable[] objects){
		double sum = 0;
		for (Measureable obj : objects) {
			sum += obj.getMeasure();
		}
		
		if (objects.length > 0) { return sum / objects.length; }
		else { return 0; }
	}
	public static Measureable min(Measureable obj1,Measureable obj2){
		if (obj1.getMeasure() < obj2.getMeasure()) {
			return obj1;
		}
		else {
			return obj2;
		}
	}
}
