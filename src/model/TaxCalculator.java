package model;

import java.util.ArrayList;
import interfaces.Taxable;

public class TaxCalculator {
	public static double sum(ArrayList<Taxable> objects){
		double sumTax = 0;
		for (Taxable taxable : objects) {
			sumTax = sumTax + taxable.getTaxable();
		}
		return sumTax;
	}
	

}
